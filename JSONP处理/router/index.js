const { asyncHandle } = require('../util')
const router = require('express').Router()
const crypt = require('../crypt')

router.get(
    '/',
    asyncHandle(async (req, res) => {
        return "响应成功"
    })
)

// 登录接口
router.post(
    '/login',
    asyncHandle(async (req, res) => {
        const result = await { id: 999 }
        const value = crypt.encrypt(result.id.toString())

        // 登录成功

        // 通过 cookie 给予：适配浏览器
        res.cookie('token', value, {
            path: "/",
            domain: 'localhost',
            maxAge: 60 * 1000, //毫秒单位
            // signed: true, // 使用加密cookie
        })
        // 通过 header 给予：适配其他终端
        res.header("authorization", value)
        return req.body
    })
)

// 获取信息
router.get(
    '/info',
    asyncHandle(async (req, res) => {
        const result = await { id: 999 }
        const value = result.id

        // 通过 cookie 给予：适配浏览器
        res.cookie('token', value, {
            path: "/",
            domain: 'localhost',
            maxAge: 60 * 1000, //毫秒单位
            // signed: true, // 使用加密cookie
        })
        // 通过 header 给予：适配其他终端
        res.header("authorization", value)

        return "消息内容"
    })
)

module.exports = router
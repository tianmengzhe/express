const { getErr } = require('./util')
const { pathToRegexp } = require('path-to-regexp')
const crypt = require('./crypt')

// 需要认证的api集合
const needTokenApi = [
    { method: "GET", path: '/api/info' },
    { method: "PUT", path: '/api/info/:id' }
]

// 用于解析token
// 验证token是否通过
module.exports = (req, res, next) => {

    // api是否需要认证
    const apis = needTokenApi.filter(({ method, path }) => {
        // 通过 path-to-regexp 库 api/info/:id 匹配 api/info/177
        const reg = pathToRegexp(path)
        return method == req.method && reg.test(req.path)
    })

    if (!apis.length) return next();

    // req.signedCookies.token
    let token = req.cookies.token || req.headers.authorization
    if (!token) {
        // 没有认证过
        return handleNoToken(req, res, next)
    }
    console.log('token', token)
    // 解密
    token = crypt.unencrypt(token)
    console.log('token', token)
    // 验证token 是否有效

    next()
}
// 处理没有认证的情况
function handleNoToken(req, res, next) {
    res.status(403).send(getErr('token验证失败', 403))
}
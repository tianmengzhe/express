const expess = require('express')
const app = expess()
const errMiddleware = require("./errMiddleware")


// 创建一个路由
const stuRouter = expess.Router()
app.use('/api/student', stuRouter)

// 请求地址为 /api/student/
stuRouter.get('/', (req, res) => {
    res.send('获取学生')
})

// 请求地址为 /api/student/
stuRouter.post('/', (req, res) => {
    res.send('添加学生')
})



// 可以捕获任何请求的错误
app.use(errMiddleware)

app.listen(3000, () => console.log('启动成功：http:127.0.0.1:3000'))


# express 路由

```js
// 创建一个路由
const stuRouter = expess.Router();

// 请求地址为 /api/student/
stuRouter.get("/", (req, res) => {
  res.send("获取学生");
});

// 请求地址为 /api/student/
stuRouter.post("/", (req, res) => {
  res.send("添加学生");
});

module.exports = stuRouter;

app.use("/api/student", require("./stuRouter"));
```

# 返回格式统一处理

```js
module.exports = {
  getErr(err = "server internal error", errCode = 500) {
    return {
      code: errCode,
      msg: err,
      data: null,
    };
  },

  getReault(data) {
    return {
      code: 0,
      msg: "ok",
      data,
    };
  },
};
```

# 处理异步错误

```js
stuRouter.post("/", await (req, res, next) => {
    try{
        const result = await addStudent()
        res.send(result)
    }catch(err){
        next(err)
    }
});
```

```js
// 异步错误封装处理
function handleWrap(handle) {
  return async (req, res, next) => {
    try {
      const result = await handle(req, res, next);
      res.sned(result);
    } catch (e) {
      next(e);
    }
  };
}

stuRouter.post(
  "/",
  handleWrap(async (req, res, next) => {
    return await addStudent();
  })
);
```

// 返回格式统一处理
module.exports = {
    getErr(err = "server internal error", errCode = 500) {
        return {
            code: errCode,
            msg: err,
            data: null
        }
    },

    getReault(data) {
        return {
            code: 0,
            msg: 'ok',
            data
        }
    },

    // 异步错误封装处理
    handleWrap: async (handle) => {
        return async (req, res, next) => {
            try {
                const result = await handle()
                res.sned(result)
            } catch (e) {
                next(e)
            }
        }
    }
}
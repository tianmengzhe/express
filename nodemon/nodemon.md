# nodemon

> https://github.com/remy/nodemon#nodemon

nodemon 是一个监视器，用于监控工程中的文件变化，如果发现文件有变化，可以执行一段脚本

--config 可设置指定配置文件
--ext 设置监听文件的后缀扩展名，如想要监听 ts 文件的变更，需设置-e ts
--exec 执行脚本
--watch 设置要监听的文件路径
--ignore 设置无需监听的文件路径

`npm i -D nodemon`

直接监听
`nodemon app.js`

配置命令行参数: 文件变化后错误信息为 端口被占用

```js
"scripts": {
    "start": "nodemon -x npm run server",
    "server": "node app.js"
},
```

配置文件 ndoemon.json 或者 配置到 package.josn 里面 nodemonConfig 属性

```js

{
    "watch": [  // 监听
        "*.js",
        "*.json"
    ],
    "ignore": [ // 忽略
        "package*.json",
        "nodemon.json",
        "node_modules",
        "public"
    ],
    "env": { // 配置临时环境变量
        "NODE_ENV": "development"
    }
}
```

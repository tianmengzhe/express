const express = require('express')
const path = require("path")
const app = express()
const errHandler = require('./errHandler')
const router = require('./router')
const cookieParser = require('cookie-parser')

const session = require("express-session")
const token = require('./token')

// 加入cookie-parser
// 会在req对象中注入cookies属性用于获取所有请求传递过来的cookie
// 会在res对象中注入cookie方法，用于设置cookie
// app.use(cookieParser("miyao")) // 传密钥
app.use(cookieParser())

app.use(session({
    secret: "加密str", // 表示对客户端的sessionID加密的密钥
    name: "sessionId"
}))

// 使用token中间件
app.use(token) // token验证

app.use(express.static(path.resolve(__dirname, 'public')))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use('/api', router)

// 错误处理
app.use(errHandler)

app.listen(3000, () => console.log('服务启动成功'))
// 使用对称加密：aes 128
// 128位的密钥
const secret = "xzoptm1anff22a5n" // getMiyao() + getMiyao()
const crypto = require("crypto")
crypto.getCiphers() // 获取所有的加密算法

function getMiyao() {
    return Math.random().toString(36).slice(-8)
}

function jm() {
    // 准备一个iv,随机向量
    const iv = Buffer.from(getMiyao() + getMiyao()) // 得到一个长度为16的buffer

    return {

        // 加密一个字符串
        encrypt(str) {
            // 创建加密算法
            const cry = crypto.createCipheriv("aes-128-cbc", secret, iv)
            // 要加密的字符串  要加密的字符类型 返回的字符类型
            let result = cry.update(str, "utf-8", "hex")
            result += cry.final("hex")
            return result
        },

        // 解密一个字符串
        unencrypt(str) {
            // 创建解密算法
            const decry = crypto.createDecipheriv("aes-128-cbc", secret, iv)
            let result = decry.update(str, "hex", "utf-8")
            result += decry.final("utf-8")
            return result
        }
    }
}

module.exports = jm()

// const ecr = jm()

// const jj = ecr.encrypt('78798')
// console.log('加密后', jj);
// console.log('解密后', ecr.unencrypt(jj));
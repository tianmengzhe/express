# session

> https://github.com/expressjs/session

![](./session原理示意图.jpg)

**cookie**

- 存储在客户端
- 优点
  - 存储在客户端，不占用服务器资源
- 缺点
  - 只能是字符串格式
  - 存储量有限
  - 数据容易被获取
  - 数据容易被篡改
  - 容易丢失

**session**

- 存储在服务器端
- 优点
  - 可以是任何格式
  - 存储量理论上是无限的
  - 数据难以被获取
  - 数据难以篡改
  - 不易丢失
- 缺点
  - 占用服务器资源

**uuid**

universal unique identity

全球唯一 id

## session 使用

安装 `npm i express-session`

```js
const app = require("express")();
const session = require("express-session");
app.use(express());
```

请求接口,返回了一个 cookie `connect.sid`,可通过属性 name 修改名称

```js
router.post(
  "/login",
  asyncHandle(async (req, res) => {
    const result = await { id: 999 };
    const value = crypt.encrypt(result.id.toString());

    console.log(req.session);

    // 登录成功
    req.session.loginUser = req.body;

    return req.body;
  })
);
```

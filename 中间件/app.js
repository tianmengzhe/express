const expess = require('express')
const app = expess()
const errMiddleware = require("./errMiddleware")

app.get('/news', (req, res, next) => {
    console.log('处理函数--1')

    throw new Error("抛出错误")

},
    (req, res, next) => {
        console.log('处理函数--2')
        next()
    })

app.get('/news', (req, res) => {
    console.log('处理函数--3')
    // res.send('响应：处理函数--3') // 如果前面已经响应，再次响应会抛出错误
})

// 可以捕获任何请求的错误
app.use(errMiddleware)

app.listen(3000, () => console.log('启动成功：http:127.0.0.1:3000'))


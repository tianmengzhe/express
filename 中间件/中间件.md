# 中间件

![](./中间件示意图.jpg)

## 当匹配到了请求后

交给第一个处理函数处理, 函数中需要手动的交给后续中间件处理

```js
app.get(
  "/news",
  (req, res, next) => {
    console.log("处理函数--1");
    next(); // 运行后续处理函数
  },
  (req, res, next) => {
    console.log("处理函数--2");
    next();
  }
);

app.get("/news", (req, res) => {
  console.log("处理函数--3");
  res.send("响应：处理函数--3");
});
```

请求 http://127.0.0.1:3000/news

输出

```js
处理函数--1
处理函数--2
处理函数--3
```

## 如果前面已经响应，不能再次响应

```js
const expess = require("express");
const app = expess();

app.get(
  "/news",
  (req, res, next) => {
    console.log("处理函数--1");

    res.send("响应：处理函数--1"); // 响应了 后续中间件也会运行

    next(); // 运行后续处理函数
  },
  (req, res, next) => {
    console.log("处理函数--2");
    next();
  }
);

app.get("/news", (req, res) => {
  console.log("处理函数--3");
  // res.send('响应：处理函数--3') // 如果前面已经响应，再次响应会抛出错误
});

app.listen(3000, () => console.log("启动成功：http:127.0.0.1:3000"));
```

Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client

## 中间件错误处理

寻找后续的错误处理中间件, 如果没有，则响应 500

```js
app.get(
  "/news",
  (req, res, next) => {
    console.log("处理函数--1");
    throw new Error("抛出错误");
    // 等同于
    // next(throw new Error("抛出错误"))
  },
  (err, req, res, next) => {
    // 有4个参数就会被认为是错误处理中间件
    // 3个参数及以下是普通中间件
    res.send("服务器错误：" + err);
  }
);
```

写一个处理错误的中间件

errMiddleware.js

```js
// 错误处理中间件
module.exports = (err, req, res, next) => {
  if (err) {
    res.status(500).send({
      code: 500,
      msg: err.msg,
    });
  } else {
    next();
  }
};
```

```js
app.get(
  "/news",
  (req, res, next) => {
    console.log("处理函数--1");
    throw new Error("抛出错误");
    // 等同于
    // next(throw new Error("抛出错误"))
  },
  require("./errMiddleware")
);
```

通常使用中间件方式

```js
const expess = require("express");
const app = expess();
const errMiddleware = require("./errMiddleware");

app.get(
  "/news",
  (req, res, next) => {
    console.log("处理函数--1");
    throw new Error("抛出错误");
  },
  (req, res, next) => {
    console.log("处理函数--2");
    res.send("响应");
  }
);

// 可以捕获任何请求的错误
app.use(errMiddleware);
app.listen(3000, () => console.log("启动成功：http:127.0.0.1:3000"));
```

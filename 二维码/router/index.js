const { asyncHandle } = require('../util')
const router = require('express').Router()

router.get(
    '/',
    asyncHandle(async (req, res) => {
        return "响应成功"
    })
)


const qrcode = require('qrcode')
// qrcode.toFile("./code.png", "abs123", err => {
//     if (err) {
//         console.log(err);
//     }
// })
// qrcode.toDataURL("http://www.baidu.com", (err, url) => {
//     console.log(url); // 生成base64
// })

router.get(
    '/qrcode',
    (req, res) => {

        res.send('二维码')
    }
)

module.exports = router
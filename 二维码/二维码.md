# 二维码

## 二维码的概念

- 矩阵点

通常是白色或黑色的小点
深色表示 1,白色表示 0

- 位置探测组

三个位于角落的嵌套矩形
用于定位二维码图片的方向

- Version

1~40 的数字
数字越大，表示整个二维码的矩阵越大
1 是 21\*21,40 是 177\*177

- mode

字符编码方式

Numeric: 只编码数字
Alphanumeric：编码数字+大写字母
Kanji：编码日文中文的文字
Byte: 类似于 Ascll 编码

选择的越往下（Numeric-->Byte），承载的数据越少

- 纠错等级

如有效二维码脏兮兮的，依然可以扫出来，靠的就是纠错能力

L(低的) M(中级) Q(质量的) H(高的)

纠错等级越高，能够表达的字符量越少

当数据不足以填充整个二维码空间，会在剩余部分存储一些冗余数据

## 生成二维码

> https://github.com/soldair/node-qrcode

node-qrcode 如果要在客户端使用的话，要自行打包,如果项目使用了 webpack,直接使用即可

> 客户端生成二维码: https://www.bootcdn.cn/qrcodejs/

### node-qrcode

### 服务端生成

`npm i qrcode`

```js
const qrcode = require("qrcode");
qrcode.toFile("./code.png", "abs123", (err) => {
  if (err) {
    console.log(err);
  }
});
```

![](./img/code.png)

```js
qrcode.toDataURL("http://www.baidu.com", (err, url) => {
  console.log(url); // 生成base64
});
```

### 客户端生成

使用 qrcodejs

> https://github.com/davidshimjs/qrcodejs

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="./css/index.css" />
  </head>

  <body>
    <div id="canvas"></div>
    <script src="https://cdn.bootcdn.net/ajax/libs/qrcodejs/1.0.0/qrcode.js"></script>
    <script>
      var qrcode = new QRCode(canvas, {
        text: "http://www.baidu.com",
        width: 128,
        height: 128,
        colorDark: "#000000",
        colorLight: "#ffffff",
        correctLevel: QRCode.CorrectLevel.H,
      });
    </script>
  </body>
</html>
```

// 错误处理中间件
const { getErr } = require('../util/util')
module.exports = (err, req, res, next) => {
    if (err) {
        res.send(getErr(err instanceof Error ? err.message : err))
    } else {
        next()
    }
}
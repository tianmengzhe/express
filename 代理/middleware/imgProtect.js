const path = require("path")
const url = require("url")
module.exports = (req, res, next) => {
    const host = req.headers.host
    let referer = req.headers.referer
    // 只处理图片
    const extname = path.extname(req.path)
    const img = ['.jpg', '.jpeg', '.png', 'gif']
    if (referer && img.includes(extname)) {
        referer = url.parse(referer).host;
        if (referer !== host) {
            // 别的网站访问资源
            // 直接拒绝
            // return res.status(500).send("不要盗用我的图片！！！");

            // 给默认图片 重写url
            req.url = "/img/logo.jpg";
        }
    }
    next();
}
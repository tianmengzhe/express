const { createProxyMiddleware } = require('http-proxy-middleware');

const context = "/data"
// 当请求以 /data 开头就转发到 http://127.0.0.1:4000/api/xxx
const proxy = createProxyMiddleware(
    /** 以什么路径开头，如果不写，匹配所有请求 */
    '/data',
    {
        target: "http://127.0.0.1:4000",
        pathRewrite: function (path, req) {
            return path.substr(context.length)
        }
    })

module.exports = proxy
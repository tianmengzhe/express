const path = require("path")
const Jimp = require("jimp")

async function test() {
    // 水印图片
    const waterPath = path.resolve(__dirname, './public/upload/water.jpg')
    // 原始图片
    const originPath = path.resolve(__dirname, './origin.jpg')
    // 加水印的图片
    const targetPath = path.resolve(__dirname, './new.jpg')

    // 读取图片
    const [water, origin] = await Promise.all([Jimp.read(waterPath), Jimp.read(originPath)])

    // 混合图片
    origin.composite(water, 0, 0, {
        mode: Jimp.BLEND_SOURCE_OVER, // 水印在原图上面
        opacitySource: 0.3, // 水印透明度
        opacityDest: 1, // 原图透明度
    })

    // 保存图片
    await origin.write(targetPath)

}

// test()

/**
 *给一张图片加水印
 * @param {*} waterFile 水印图片
 * @param {*} originFile 原始图片
 * @param {*} targetPath 目标图片地址
 * @param {*} proportion 比例
 * @param {*} marginProportion 水印图片在目标图片位置的比例
 */
async function mark(waterFile, originFile, targetPath, proportion = 10, marginProportion = 0.05) {
    const [water, origin] = await Promise.all([Jimp.read(waterFile), Jimp.read(originFile)])

    // 对水印图片进行缩放
    // 原始图宽度/水印图宽度 = 10/2 = 5
    // 目标：10/1 = 10
    // 水印图要缩小 2
    const curProportion = origin.bitmap.width / water.bitmap.width;
    water.scale(curProportion / proportion)

    // 计算位置
    const right = origin.bitmap.width * marginProportion
    const bottom = origin.bitmap.height * marginProportion
    const x = origin.bitmap.width - right - water.bitmap.width
    const y = origin.bitmap.height - bottom - water.bitmap.height

    // 混合图片,写入水印
    origin.composite(water, x, y, {
        mode: Jimp.BLEND_SOURCE_OVER, // 水印在原图上面
        opacitySource: 0.3, // 水印透明度
        opacityDest: 1, // 原图透明度
    })

    // 保存图片
    await origin.write(targetPath)
}

async function test2() {
    const waterPath = path.resolve(__dirname, './public/upload/water.jpg')
    const originPath = path.resolve(__dirname, './origin.jpg')
    const targetPath = path.resolve(__dirname, './new.jpg')
    await mark(waterPath, originPath, targetPath, 5, 0.01)
}

// test2()
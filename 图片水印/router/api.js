const Router = require('express').Router()
const { asyncHandle, getErr } = require('../util/util')
const { publish, verify } = require('../util/jwt')

Router.get('/login', asyncHandle(async (req, res, next) => {
    publish(res, { id: 99 })
    return '登录成功'
}))

Router.get('/info', asyncHandle(async (req, res, next) => {
    const resule = verify(req)
    console.log('resule', resule)
    if (resule) {
        return resule
    } else {
        return 'token 验证失败'
    }

}))

// 获取用户信息
Router.get('/whoami', asyncHandle(async (req, res, next) => {
    const resule = verify(req)
    console.log('resule', resule)
    if (resule) {
        // resule.id // 去获取用户数据
        return { name: "张三", age: '18', id: resule.id }
    } else {
        res.send(getErr('token 验证失败'))
    }
}))


module.exports = Router
const expess = require('express')
const app = expess()

app.on('*', (req, res, next) => {
    next()
})

// 配置一个请求映射
app.get('/list', (req, res) => {
    // req, res 是被express封装后的对象
    console.log('请求头:', req.headers)
    console.log('请求路径:', req.path)
    console.log('url参数:', req.query)
    res.send('列表')
})

// 动态请求 请求新闻  匹配 /news/12  /news/ll
app.get('/news/:id', (req, res) => {
    console.log('参数:', req.params)
    res.send(JSON.stringify(req.params))
})

// 多段动态id
// info/张三/18
app.get('/info/:name/:age', (req, res) => {
    console.log('参数:', req.params)

    // // 设置响应头
    // res.setHeader('a', 123)

    // // 响应
    // res.send(req.params)

    // 重定向
    // res.status(302).header('location', "http://www.baidu.com").end()
    // 简写
    // res.status(302).location("http://www.baidu.com")
    // 简写
    res.redirect(302, "http://www.baidu.com")
})
app.listen(3000, () => console.log('启动成功：http:127.0.0.1:3000'))
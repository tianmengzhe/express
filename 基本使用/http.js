const http = require('http')
const expess = require('express')

// 创建一个expess应用  
// app实际上是一个函数，用于处理请求的函数
const app = expess()

const server = http.createServer(app)

server.on('request', (req, res) => {
    console.log('--server--')
    res.setHeader('Content-Type', 'text/plain;charset=utf-8')
    res.write('响应')
})

server.listen(4000, () => console.log('启动成功：http:127.0.0.1:4000'))

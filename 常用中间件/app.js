const expess = require('express')
const app = expess()
const path = require("path")

// 静态资源目录
const staticRoot = path.resolve(__dirname, 'public')
/**
 * 当请求时，会根据请求路径(req.path)，从指定的目录中寻找是否存在该文件，
 * 如果存在，直接响应文件内容，不在移交给后续的中间件
 * 如果不存在文件，则直接移交给后续的中间件处理
 * 如果是一个目录，默认会自动使用index.html文件，第二个参数可配置
 */
// app.use(expess.static(staticRoot))
// http://127.0.0.1:3000/
// http://127.0.0.1:3000/css/index.css

// 请求路径必须以public开头
app.use('/public', expess.static(staticRoot))
// http://127.0.0.1:3000/public/
// http://127.0.0.1:3000/public/css/index.css

// http://127.0.0.1:3000/public/css/index.css
// app.use('/public', (req, res) => {
//     // req.baseUrl /public
//     // req.path /css/index.css
// })

app.use(expess.urlencoded({ extended: true }))


// app.use(require('./myUrlEncoded'))

app.use(expess.json())

app.post('/post', (req, res, next) => {
    console.log('body', req.headers)
    res.send(req.body)
})

app.get('/news', (req, res, next) => {
    res.send('')
})

app.listen(3000, () => console.log('启动成功：http:127.0.0.1:3000'))


const Router = require('express').Router()
const api = require('./api')
const upLoad = require('./upLoad')

Router.use('*', (req, res, next) => {
    console.log("请求路径", req.baseUrl)
    next()
})

Router.use('/api', api)
Router.use('/upLoad', upLoad)

module.exports = Router
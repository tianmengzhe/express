module.exports = {

    getErr(err = "server internal error", errCode = 500) {
        return {
            code: errCode,
            msg: err,
            data: null
        }
    },

    reault(data) {
        return {
            code: 0,
            msg: 'ok',
            data
        }
    },

    // 异步错误封装处理
    asyncHandle(handler) {
        return async (req, res, next) => {
            try {
                const result = await handler(req, res, next)
                res.send(module.exports.reault(result))
            } catch (err) {
                next(err)
            }
        }
    }
}
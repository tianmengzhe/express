const express = require('express')
const path = require("path")
const cookieParser = require('cookie-parser')
const history = require('connect-history-api-fallback');
const app = express()
const api = require("./router")
const err = require("./middleware/err")
// app.use(history())

app.use(cookieParser())
app.use(express.static(path.resolve(__dirname, 'public')))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(api)
app.use(err)


app.listen(4000, () => console.log('http:127.0.0.1:4000'))
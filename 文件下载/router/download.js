const { log } = require('console')
const path = require('path')
const Router = require('express').Router()

// 下载文件
Router.get('/:filename', (req, res) => {
    // 文件绝对路径
    const absPath = path.resolve(__dirname, '../public/upload', req.params.filename)
    // 保存的文件名
    const saveName = req.params.filename
    // 第三个传输：错误处理
    // (err) => console.log('错误处理', err}
    res.download(absPath, saveName)
})


module.exports = Router
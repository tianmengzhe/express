const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
    const sex = req.query.sex || -1;
    const name = req.query.name || "";
    const result = {
        total: 100,
        datas: [
            { name: "学生一" },
            { name: "学生二" }
        ]
    }
    res.render("./students.ejs", {
        ...result,
        page,
        limit,
    });
});

module.exports = router;

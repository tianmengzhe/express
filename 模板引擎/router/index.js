const { asyncHandle } = require('../util')
const router = require('express').Router()

router.get(
    '/',
    asyncHandle(async (req, res) => {
        return "响应成功"
    })
)

module.exports = router
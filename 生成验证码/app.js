const express = require('express')
const path = require("path")
const app = express()
const errHandler = require('./errHandler')
const router = require('./router')
const cookieParser = require('cookie-parser')

app.use(cookieParser())

app.use(require("express-session")({
    secret: "加密session"
}))

app.use(express.static(path.resolve(__dirname, 'public')))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use('/api', router)

// 错误处理
app.use(errHandler)

app.listen(3000, () => console.log('服务启动成功: localhost:3000'))
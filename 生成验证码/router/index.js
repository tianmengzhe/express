const { asyncHandle } = require('../util')
const router = require('express').Router()
var svgCaptcha = require('svg-captcha');


router.post("*", captchcHandler)
router.put("*", captchcHandler)

router.post(
    '/login',
    asyncHandle(async (req, res) => {
        // 简易处理
        req.session.loginUser = req.body

        // 验证验证码

        return req.body
    })
)

router.get('/captcha', (req, res) => {
    // var c = svgCaptcha.create({
    //     size: 4, // 验证码长度
    //     ignoreChars: "0o1i", // 验证码字符中排除 0o1i
    //     noise: 4, // 干扰线条的数量
    //     color: true, // 验证码的字符是否有颜色，默认没有，如果设定了背景，则默认有
    //     background: "#cc9966", // 验证码图片背景颜色
    // });
    // 生成了一个svg图片 {text:"xxx",data:"<svg ....>"}

    // 生成数学公式并且有颜色的验证码
    var c = svgCaptcha.createMathExpr({
        color: true,
        background: "#d0d0d0",
    })
    // 把验证码中的文本放到session中
    req.session.captcha = c.text.toLowerCase()
    res.type("svg")
    res.send(c.data)
})

// 简单验证
router.get('/verify/:c', asyncHandle(async (req, res) => {
    const c = req.params.c
    let result = req.session.captcha === c ? '通过' : "不通过"
    return "验证：" + result
}))

// 需不需要验证验证码
function captchcHandler(req, res, next) {
    if (!req.session.records) { // 如果中没有访问记录
        req.session.records = []
    }
    // 把这一次请求的访问时间记录下来 
    const now = new Date().getTime()
    req.session.records.push(now)

    // 在一段时间中请求到达了一定数量，就可能时机器
    const duration = 10000
    const reqpeat = 3
    req.session.records = req.session.records.filter(timer => now - timer <= duration)
    // 到达了一定数量 或者 提交了验证码
    if (req.session.records.length > reqpeat || "captcha" in req.body) {
        captchaVerify(req, res, next)
    } else {
        next()
    }
}

function captchaVerify(req, res, next) {
    const reqCaptcha = req.body.captcha ? req.body.captcha.toLowerCase() : '';
    if (req.session.captcha === reqCaptcha) {
        // 验证码有问题
        res.send({
            code: 401,
            msg: "验证码有问题"
        })
    } else {
        next()
    }
    // 验证码一旦比较过，就失效
    req.session.captcha = ""
}

module.exports = router
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 在网站被访问时，需要用token去换取用户信息
store.dispatch('loginUser/whoAmi')

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: function (h) { return h(App) }
}).$mount('#app')

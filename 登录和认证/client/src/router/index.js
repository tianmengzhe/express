import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from "../store"

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
        beforeEnter(to, from, next) {
            if (store.state.loginUser.data) { // 已经登录
                next()
            } else {
                // 没有登录，调整login
                next({ path: '/login' })
            }
        }
    },
    {
        path: '/login',
        component: () => import(/* webpackChunkName: "login" */ '../views/login.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router

import request from "./request";

function delay(time) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, time)
    })
}

export async function login(obj) {
    await delay(2000)
    return await request.get('api/login', obj).then(res => res.data)
}

export function logout(obj) {
    localStorage.removeItem("token")
}

export async function whoAmi() {
    await delay(2000)
    return await request.get('api/whoami').then(res => res.data)
}
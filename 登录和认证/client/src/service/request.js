
import axios from "axios";

// 1.发送请求的时候，如果有cookie,需要附带到响应头中
// 2.响应的时候，如果有token,保存token到本地（localstorage）
// 3.如果响应码是403（没有token,token失效），清除本地token

const install = axios.create()

install.interceptors.request.use(config => {
    let token = localStorage.getItem('token')
    if (token) {
        config.headers['authorization'] = token
    }
    return config
})

install.interceptors.response.use(response => {
    let token = response.headers.authorization
    if (token) {
        localStorage.setItem('token', token)
    }
    return response
}, err => {
    if (err.response.status == 403) {
        localStorage.removeItem('token')
    }
    return Promise.reject(err)
})

export default install
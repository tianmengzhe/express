import { login, logout, whoAmi } from "../service/loginServce";
export default {
    namespaced: true,
    state: {
        data: null,
        isLoading: false // 是否登录中
    },
    mutations: {
        setData(state, payload) {
            state.data = payload
        },
        setIsLoading(state, payload) {
            state.isLoading = payload
        },
    },
    actions: {
        async login({ commit }, payload) {
            commit('setIsLoading', true)
            const { data } = await login(payload)
            commit('setData', data)
            commit('setIsLoading', false)
        },
        logout({ commit }) {
            commit('setData', null)
            logout()
        },
        async whoAmi({ commit }) {
            commit('setIsLoading', true)
            try {
                const { data } = await whoAmi()
                commit('setData', data)
            } catch (e) {
                commit('setData', null)
            }
            commit('setIsLoading', false)
        }
    }
}
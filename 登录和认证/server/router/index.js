const Router = require('express').Router()
const api = require('./api')

Router.use('/api', api)

module.exports = Router
# 登录和验证

## 使用 cookie-parser

> https://github.com/expressjs/cookie-parser#readme

`npm i cookie-parser`

```js
const cookieParser = require("cookie-parser");

// 加入cookie-parser
// 会在req对象中注入cookies属性用于获取所有请求传递过来的cookie
// 会在res对象中注入cookie方法，用于设置cookie
app.use(cookieParser());
```

## 登录成功后给与 token

通过 cookie 给予：适配浏览器

通过 header 给予：适配其他终端

```js
router.post(
  "/login",
  asyncHandle(async (req, res) => {
    const result = await { id: 999 };
    const value = result.id;

    // 登录成功

    // 通过 cookie 给予：适配浏览器
    res.cookie("token", value, {
      path: "/",
      domain: "localhost",
      maxAge: 60 * 1000, //毫秒单位
    });
    // 通过 header 给予：适配其他终端
    res.header("authorization", value);
    return req.body;
  })
);
```

## 对后续请求进行认证

```js
const { getErr } = require("./util");
const { pathToRegexp } = require("path-to-regexp");

// 需要认证的api集合
const needTokenApi = [
  { method: "GET", path: "/api/info" },
  { method: "PUT", path: "/api/info/:id" },
];

// 用于解析token
// 验证token是否通过
module.exports = (req, res, next) => {
  console.log("token", req.method, req.path);
  // api是否需要认证
  const apis = needTokenApi.filter(({ method, path }) => {
    // 通过 path-to-regexp 库 api/info/:id 匹配 api/info/177
    const reg = pathToRegexp(path);
    return method == req.method && reg.test(req.path);
  });

  if (!apis.length) return next();

  let token = req.cookies.token || req.headers.authorization;
  if (!token) {
    // 没有认证过
    return handleNoToken(req, res, next);
  }
  // 验证token 是否有效

  next();
};
// 处理没有认证的情况
function handleNoToken(req, res, next) {
  res.status(403).send(getErr("token验证失败", 403));
}
```

## 使用加密的 cookie

```js
app.use(cookieParser("miyao")); // 传密钥

// 设置cookie
res.cookie("token", value, {
  path: "/",
  domain: "localhost",
  maxAge: 60 * 1000, //毫秒单位
  signed: true, // 使用加密cookie
});

// 获取cookie
req.signedCookies.token;
```

## 手动加密

```js
// 使用对称加密：aes 128
// 128位的密钥
const secret = "xzoptm1anff22a5n"; // getMiyao() + getMiyao()
const crypto = require("crypto");
crypto.getCiphers(); // 获取所有的加密算法

function getMiyao() {
  return Math.random().toString(36).slice(-8);
}

// 准备一个iv,随机向量
const iv = Buffer.from(getMiyao() + getMiyao()); // 得到一个长度为16的buffer

function jm() {
  return {
    // 加密一个字符串
    encrypt(str) {
      // 创建加密算法
      const cry = crypto.createCipheriv("aes-128-cbc", secret, iv);
      // 要加密的字符串  要加密的字符类型 返回的字符类型
      let result = cry.update(str, "utf-8", "hex");
      result += cry.final("hex");
      return result;
    },

    // 解密一个字符串
    unencrypt(str) {
      // 创建解密算法
      const decry = crypto.createDecipheriv("aes-128-cbc", secret, iv);
      let result = decry.update(str, "hex", "utf-8");
      result += decry.final("utf-8");
      return result;
    },
  };
}

module.exports = jm;

// const ecr = jm()

// const jj = ecr.encrypt('78798')
// console.log('加密后', jj);
// console.log('解密后', ecr.unencrypt(jj));
```
